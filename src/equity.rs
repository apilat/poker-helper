use crate::poker::{Card, CardSet, Evaluation};

#[derive(Debug, Clone)]
pub struct Equity {
    pub equity: f64,
    pub win: u64,
    pub draw: u64,
}

pub fn calculate_equity(hands: &[CardSet]) -> Vec<Equity> {
    let n = hands.len();
    if n == 0 {
        return vec![];
    }

    let mut any_hand = CardSet::empty();
    for &hand in hands {
        assert!(hand.cards() == 2, "hand must have exactly two cards");
        any_hand += hand;
    }
    assert!(
        any_hand.cards() as usize == n * 2,
        "hands cannot share cards"
    );

    let mut equity = vec![(0, 0); n];
    let mut evals = vec![Evaluation::default(); n];
    let mut total = 0;

    let cards = (0..52)
        .map(|x| Card::from_idx(x).unwrap())
        .collect::<Vec<_>>();
    for i1 in 0..52 {
        for i2 in i1 + 1..52 {
            for i3 in i2 + 1..52 {
                for i4 in i3 + 1..52 {
                    for i5 in i4 + 1..52 {
                        let board = CardSet::empty()
                            + cards[i1]
                            + cards[i2]
                            + cards[i3]
                            + cards[i4]
                            + cards[i5];
                        if hands.iter().any(|&hand| (board & hand).cards() > 0) {
                            continue;
                        }

                        for (eval, &hand) in evals.iter_mut().zip(hands.iter()) {
                            *eval = Evaluation::of(board + hand);
                        }

                        let best = evals.iter().max().unwrap();
                        let is_draw = evals.iter().filter(|&x| x == best).count() > 1;
                        for (equity, eval) in equity.iter_mut().zip(evals.iter()) {
                            if eval == best {
                                if is_draw {
                                    equity.1 += 1;
                                } else {
                                    equity.0 += 1;
                                }
                            }
                        }
                        total += 1;
                    }
                }
            }
        }
    }

    equity
        .into_iter()
        .map(|(win, draw)| Equity {
            win,
            draw,
            equity: (win as f64 + draw as f64 * 0.5) / total as f64,
        })
        .collect()
}
