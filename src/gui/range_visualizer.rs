use crate::poker::{Hand, HandMap};
use druid::piet::{Text, TextLayout, TextLayoutBuilder};
use druid::widget::{Flex, Label};
use druid::{
    AppLauncher, BoxConstraints, Color, Data, Env, Event, EventCtx, LayoutCtx, Lens, LifeCycle,
    LifeCycleCtx, PaintCtx, Point, Rect, RenderContext, Size, UpdateCtx, Widget, WidgetExt,
    WindowDesc,
};

#[derive(Debug, Clone, PartialEq, Lens)]
struct State {
    range: HandMap<bool>,
}

impl State {
    fn new() -> State {
        State {
            range: HandMap::new(),
        }
    }
}

impl Data for State {
    fn same(&self, other: &Self) -> bool {
        self == other
    }
}

impl Data for Hand {
    fn same(&self, other: &Self) -> bool {
        self == other
    }
}

impl Data for HandMap<bool> {
    fn same(&self, other: &Self) -> bool {
        self == other
    }
}

#[derive(Debug, Clone, PartialEq, Lens)]
struct CurrentOperation {
    mouse_pos: Point,
    removing: bool,
}

#[derive(Debug, Clone)]
struct RangeWidget {
    size: f64,
    op: Option<CurrentOperation>,
}

impl RangeWidget {
    fn new() -> RangeWidget {
        RangeWidget {
            size: 0.0,
            op: None,
        }
    }
}

impl Widget<HandMap<bool>> for RangeWidget {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut HandMap<bool>, _env: &Env) {
        match event {
            Event::MouseDown(evt) => {
                self.op = Some(CurrentOperation {
                    mouse_pos: evt.pos,
                    removing: evt.buttons.contains(druid::MouseButton::Right),
                });
                ctx.set_active(true);
            }
            Event::MouseUp(_) => {
                self.op = None;
                ctx.set_active(false);
            }
            Event::MouseMove(evt) => {
                if let Some(op) = &mut self.op {
                    op.mouse_pos = evt.pos;
                }
            }
            _ => (),
        }

        if let Some(CurrentOperation {
            mouse_pos,
            removing,
        }) = &self.op
        {
            if 0.0 <= mouse_pos.x
                && mouse_pos.x < self.size
                && 0.0 <= mouse_pos.y
                && mouse_pos.y < self.size
            {
                let (x, y) = (
                    (mouse_pos.x / self.size * 13.0) as u8,
                    (mouse_pos.y / self.size * 13.0) as u8,
                );
                let hand = Hand::from_grid_pos(x, y).unwrap();
                let current = *data.get(&hand);

                if !removing ^ current {
                    *data.get_mut(&hand) = !current;
                    ctx.request_paint();
                }
            }
        }
    }

    fn lifecycle(
        &mut self,
        _ctx: &mut LifeCycleCtx,
        _event: &LifeCycle,
        _data: &HandMap<bool>,
        _env: &Env,
    ) {
    }

    fn update(
        &mut self,
        ctx: &mut UpdateCtx,
        _old_data: &HandMap<bool>,
        _data: &HandMap<bool>,
        _env: &Env,
    ) {
        ctx.request_paint();
    }

    fn layout(
        &mut self,
        _ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        _data: &HandMap<bool>,
        _env: &Env,
    ) -> Size {
        self.size = f64::min(bc.max().width, bc.max().height);
        Size::new(self.size, self.size)
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &HandMap<bool>, _env: &Env) {
        for x in 0..13 {
            for y in 0..13 {
                let hand = Hand::from_grid_pos(x, y).unwrap();
                let color = if *data.get(&hand) {
                    Color::BLUE
                } else {
                    Color::GRAY
                };

                ctx.fill(
                    Rect::new(
                        x as f64 / 13.0 * self.size,
                        y as f64 / 13.0 * self.size,
                        (x + 1) as f64 / 13.0 * self.size,
                        (y + 1) as f64 / 13.0 * self.size,
                    ),
                    &color,
                );

                let text = ctx
                    .text()
                    .new_text_layout(hand.abbr().to_string())
                    .build()
                    .expect("cannot create text");
                let center = Point::new(
                    (x as f64 + 0.5) / 13.0 * self.size,
                    (y as f64 + 0.5) / 13.0 * self.size,
                );
                ctx.draw_text(&text, center - text.size().to_vec2() * 0.5);
            }
        }
    }
}

fn description_widget() -> impl Widget<HandMap<bool>> {
    Flex::column().with_child(Label::dynamic(|state: &HandMap<bool>, _env| {
        let combos = Hand::iter()
            .filter(|x| *state.get(x))
            .map(|h| h.combos())
            .sum::<usize>();
        format!("Combos: {} / 1326 ({:.2}%)", combos, combos as f32 / 1326.0 * 100.0)
    }))
}

fn build_app() -> impl Widget<State> {
    // TODO fix layout when resizing
    Flex::row()
        .with_child(RangeWidget::new().lens(State::range))
        .with_child(description_widget().lens(State::range).center())
}

pub fn launch_app() -> Result<(), druid::PlatformError> {
    let window = WindowDesc::new(build_app).title("Poker helper - Range visualizer");
    let state = State::new();

    AppLauncher::with_window(window)
        .use_simple_logger()
        .launch(state)
}
