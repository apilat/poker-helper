use crate::poker::{Card, CardSet, Evaluation, Rank, Suit};
use druid::widget::{Button, Container, Either, Flex, Label, Painter};
use druid::{AppLauncher, Color, Data, Lens, RenderContext, Widget, WidgetExt, WindowDesc};
use rand::thread_rng;
use std::cmp;
use strum::IntoEnumIterator;

const CARD_COUNT: usize = 5;
#[derive(Debug, Clone, PartialEq, Eq, Lens)]
struct State {
    board: [Card; CARD_COUNT],
    selected_cards: [Option<Card>; 2],
    status_msg: String,
    guess_done: bool,
}

impl State {
    pub fn new() -> State {
        State {
            board: {
                let indices = rand::seq::index::sample(&mut thread_rng(), Card::COUNT, CARD_COUNT);
                let mut cards = [Card::from_idx(0).unwrap(); CARD_COUNT];
                for (i, c) in cards.iter_mut().enumerate() {
                    *c = Card::from_idx(indices.index(i)).unwrap();
                }
                cards
            },
            selected_cards: [None; 2],
            status_msg: "Select the best hole cards".into(),
            guess_done: false,
        }
    }

    pub fn reset(&mut self) {
        *self = State::new();
    }

    pub fn is_currently_selected(&self, card: Card) -> bool {
        self.selected_cards.iter().any(|&sel| Some(card) == sel)
    }

    pub fn is_on_board(&self, card: Card) -> bool {
        self.board.iter().any(|&c| c == card)
    }

    pub fn toggle_select(&mut self, card: Card) {
        if self.is_on_board(card) {
            return;
        }

        if let Some(sel) = self.selected_cards.iter_mut().find(|x| **x == Some(card)) {
            *sel = None;
        } else if let Some(sel) = self.selected_cards.iter_mut().find(|x| x.is_none()) {
            *sel = Some(card);
        } else {
            // two other cards selected, currently ignoring
        }
    }

    pub fn verify(&mut self) {
        let hand = match self.selected_cards {
            [Some(x), Some(y)] => CardSet::empty() + x + y,
            _ => return,
        };
        let board = self.board.iter().fold(CardSet::empty(), |x, &y| x + y);
        let evaluation = Evaluation::of(board | hand);

        let mut best_hand = None;
        for hc1 in Card::iter() {
            for hc2 in Card::iter() {
                let other_hand = CardSet::empty() + hc1 + hc2;
                if (board & other_hand).cards() > 0 || (hand & other_hand).cards() > 0 {
                    continue;
                }

                let ev = Evaluation::of(board | other_hand);
                if let Some(x) = best_hand {
                    best_hand = Some(cmp::max_by_key(x, (hc1, hc2, ev), |x| x.2));
                } else {
                    best_hand = Some((hc1, hc2, ev));
                }
            }
        }

        let (hc1, hc2, best_eval) = best_hand.unwrap();
        if best_eval > evaluation {
            self.status_msg = format!(
                "Incorrect! {}{} is best ({})",
                hc1.abbr(),
                hc2.abbr(),
                best_eval.decode(),
            );
        } else {
            self.status_msg = "Correct!".into();
        }
        self.guess_done = true;
    }
}

impl Data for State {
    fn same(&self, other: &State) -> bool {
        self == other
    }
}

impl Data for Card {
    fn same(&self, other: &Self) -> bool {
        self == other
    }
}

fn card_color(card: Card) -> Color {
    match card.suit() {
        Suit::Spades => Color::rgb8(25, 25, 25),
        Suit::Hearts => Color::rgb8(255, 25, 25),
        Suit::Clubs => Color::rgb8(0, 102, 17),
        Suit::Diamonds => Color::rgb8(0, 153, 230),
    }
}

fn board_card_widget() -> impl Widget<Card> {
    Container::new(
        Label::dynamic(move |card: &Card, _env| card.rank().abbr().to_string())
            .with_text_size(32.0)
            .center(),
    )
    .background(Painter::new(move |ctx, &card: &Card, _env| {
        let color = card_color(card);
        let bounds = ctx.size().to_rect();
        ctx.fill(bounds, &color);
    }))
    .rounded(20.0)
    .fix_size(60.0, 100.0)
}

fn selection_card_widget(card: Card) -> impl Widget<State> {
    Container::new(
        Label::new(card.rank().abbr().to_string())
            .with_text_size(14.0)
            .center(),
    )
    .background(Painter::new(move |ctx, state: &State, _env| {
        let color = if state.is_on_board(card) {
            Color::GRAY
        } else {
            card_color(card)
        };
        let bounds = ctx.size().to_rect();
        let inner_bounds = bounds.to_rounded_rect(8.0);
        ctx.fill(inner_bounds, &color);

        if state.is_currently_selected(card) {
            let border_bounds = bounds.inset(-1.5).to_rounded_rect(8.0);
            ctx.stroke(border_bounds, &Color::WHITE, 3.0)
        }
    }))
    .fix_size(20.0, 33.3)
    .on_click(move |_ctx, state: &mut State, _env| state.toggle_select(card))
}

fn board_widget() -> impl Widget<[Card; CARD_COUNT]> {
    let mut flex = Flex::row();
    for i in 0..CARD_COUNT {
        if i > 0 {
            flex.add_default_spacer();
        }
        flex.add_child(board_card_widget().lens(druid::lens::Field::new(
            move |x: &[Card; CARD_COUNT]| &x[i],
            move |x| &mut x[i],
        )));
    }
    flex
}

fn card_selection() -> impl Widget<State> {
    let mut flex = Flex::column();
    for (i, s) in Suit::iter().enumerate() {
        let mut row = Flex::row();

        for (j, r) in Rank::iter().enumerate() {
            if j > 0 {
                row.add_default_spacer();
            }
            row.add_child(selection_card_widget(Card::new(r, s)));
        }

        if i > 0 {
            flex.add_default_spacer();
        }
        flex.add_child(row);
    }
    flex
}

fn build_app() -> impl Widget<State> {
    Flex::column()
        .with_child(board_widget().lens(State::board))
        .with_default_spacer()
        .with_child(card_selection())
        .with_default_spacer()
        .with_child(Either::new(
            |state: &State, _env| state.guess_done,
            Button::new("Continue").on_click(|_ctx, state: &mut State, _env| state.reset()),
            Button::new("Verify").on_click(|_ctx, state: &mut State, _env| state.verify()),
        ))
        .with_default_spacer()
        .with_child(Label::dynamic(|state: &State, _env| {
            state.status_msg.clone()
        }))
        .center()
}

pub fn launch_app() -> Result<(), druid::PlatformError> {
    let window = WindowDesc::new(build_app).title("Poker helper - Nut quiz");
    let state = State::new();

    AppLauncher::with_window(window)
        .use_simple_logger()
        .launch(state)
}
