use num_enum::{IntoPrimitive, TryFromPrimitive};
use once_cell::sync::Lazy;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::convert::{TryFrom, TryInto};
use std::fmt::{self, Display, Formatter};
use std::num::NonZeroU8;
use std::ops::{Add, AddAssign, BitAnd, BitOr, BitXor, Sub, SubAssign};
use std::str::FromStr;
use strum::EnumCount;
use strum_macros::{Display, EnumCount, EnumDiscriminants, EnumIter};

#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    Hash,
    Display,
    EnumCount,
    IntoPrimitive,
    TryFromPrimitive,
    EnumIter,
)]
#[repr(u8)]
#[strum(serialize_all = "snake_case")]
pub enum Suit {
    Hearts,
    Clubs,
    Spades,
    Diamonds,
}

impl Suit {
    pub fn abbr(&self) -> char {
        use Suit::*;
        match self {
            Hearts => 'h',
            Clubs => 'c',
            Spades => 's',
            Diamonds => 'd',
        }
    }

    pub fn from_abbr(x: char) -> Option<Suit> {
        use Suit::*;
        match x.to_ascii_lowercase() {
            'h' => Some(Hearts),
            'c' => Some(Clubs),
            's' => Some(Spades),
            'd' => Some(Diamonds),
            _ => None,
        }
    }
}

#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    Display,
    EnumCount,
    IntoPrimitive,
    TryFromPrimitive,
    EnumIter,
)]
#[repr(u8)]
#[strum(serialize_all = "snake_case")]
pub enum Rank {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}

impl Rank {
    pub fn abbr(&self) -> char {
        use Rank::*;
        match self {
            Two => '2',
            Three => '3',
            Four => '4',
            Five => '5',
            Six => '6',
            Seven => '7',
            Eight => '8',
            Nine => '9',
            Ten => 'T',
            Jack => 'J',
            Queen => 'Q',
            King => 'K',
            Ace => 'A',
        }
    }

    pub fn from_abbr(x: char) -> Option<Rank> {
        use Rank::*;
        match x.to_ascii_uppercase() {
            '2' => Some(Two),
            '3' => Some(Three),
            '4' => Some(Four),
            '5' => Some(Five),
            '6' => Some(Six),
            '7' => Some(Seven),
            '8' => Some(Eight),
            '9' => Some(Nine),
            'T' => Some(Ten),
            'J' => Some(Jack),
            'Q' => Some(Queen),
            'K' => Some(King),
            'A' => Some(Ace),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Card(u8);

impl Card {
    pub const COUNT: usize = 52;

    #[inline]
    pub fn new(rank: Rank, suit: Suit) -> Card {
        Card((suit as usize * Rank::COUNT + rank as usize) as u8)
    }

    #[inline]
    pub fn suit(&self) -> Suit {
        Suit::try_from(self.0 / Rank::COUNT as u8).unwrap()
    }

    #[inline]
    pub fn rank(&self) -> Rank {
        Rank::try_from(self.0 % Rank::COUNT as u8).unwrap()
    }

    #[inline]
    pub fn idx(&self) -> usize {
        self.0 as usize
    }

    pub fn from_idx(x: usize) -> Option<Self> {
        if x < Card::COUNT {
            Some(Card(x as u8))
        } else {
            None
        }
    }

    pub fn abbr(&self) -> impl Display {
        // TODO match statement to return &'static str
        struct Tmp(Rank, Suit);
        impl Display for Tmp {
            fn fmt(&self, f: &mut Formatter) -> fmt::Result {
                write!(f, "{}{}", self.0.abbr(), self.1.abbr())
            }
        }
        Tmp(self.rank(), self.suit())
    }

    pub fn iter() -> impl DoubleEndedIterator<Item = Card> {
        (0..Card::COUNT).map(|x| Card::from_idx(x).unwrap())
    }
}

impl Display for Card {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{} of {}", self.rank(), self.suit())
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct CardSet {
    // bit field indexed by Card::idx()
    mask: u64,
}

impl CardSet {
    #[inline]
    pub fn empty() -> Self {
        CardSet { mask: 0 }
    }

    #[inline]
    pub fn contains(&self, card: Card) -> bool {
        (self.mask & (1 << card.idx())) > 0
    }

    #[inline]
    pub fn cards(&self) -> u8 {
        self.mask.count_ones() as u8
    }
}

impl FromStr for CardSet {
    // TODO Proper error type
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut set = CardSet::empty();
        let mut it = s.chars();
        while let Some(r) = it.next() {
            let s = it
                .next()
                .ok_or_else(|| "card set length must be odd".to_string())?;
            set += Card::new(
                Rank::from_abbr(r).ok_or_else(|| format!("invalid rank {}", r))?,
                Suit::from_abbr(s).ok_or_else(|| format!("invalid suit {}", s))?,
            );
        }
        Ok(set)
    }
}

impl From<Card> for CardSet {
    #[inline]
    fn from(card: Card) -> Self {
        CardSet::empty() + card
    }
}

impl Add<Card> for CardSet {
    type Output = CardSet;
    #[inline]
    fn add(mut self, rhs: Card) -> Self::Output {
        self += rhs;
        self
    }
}

impl AddAssign<Card> for CardSet {
    #[inline]
    fn add_assign(&mut self, rhs: Card) {
        self.mask |= 1 << rhs.idx();
    }
}

impl Add<CardSet> for CardSet {
    type Output = CardSet;
    #[inline]
    fn add(mut self, rhs: Self) -> Self::Output {
        self += rhs;
        self
    }
}

impl AddAssign for CardSet {
    #[inline]
    #[allow(clippy::suspicious_op_assign_impl)]
    fn add_assign(&mut self, rhs: Self) {
        self.mask |= rhs.mask;
    }
}

impl Sub<Card> for CardSet {
    type Output = CardSet;
    #[inline]
    fn sub(mut self, rhs: Card) -> Self::Output {
        self -= rhs;
        self
    }
}

impl SubAssign<Card> for CardSet {
    #[inline]
    fn sub_assign(&mut self, rhs: Card) {
        self.mask &= !(1 << rhs.idx());
    }
}

impl BitAnd for CardSet {
    type Output = CardSet;
    #[inline]
    fn bitand(mut self, rhs: Self) -> Self::Output {
        self.mask &= rhs.mask;
        self
    }
}

impl BitOr for CardSet {
    type Output = CardSet;
    #[inline]
    fn bitor(mut self, rhs: Self) -> Self::Output {
        self.mask |= rhs.mask;
        self
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Evaluation {
    // rrrr00AKJT98765432akjt98765432
    // rrrr = strength of hand
    // AKJ..2 = playing
    // akj..2 = kicker
    val: u32,
}

impl Evaluation {
    pub fn decode(self) -> EvaluationDecoded {
        let (mut playing, mut kickers) = (Vec::new(), Vec::new());
        for r in (0..Rank::COUNT as u8).rev() {
            if (self.val & (1 << (13 + r))) > 0 {
                playing.push(Rank::try_from(r).unwrap());
            }
            if (self.val & (1 << r)) > 0 {
                kickers.push(Rank::try_from(r).unwrap());
            }
        }

        use EvaluationDecoded::*;
        match self.val >> 28 {
            9 => StraightFlush { rank: playing[0] },
            8 => FourOfAKind {
                rank: playing[0],
                kicker: kickers[0],
            },
            7 => FullHouse {
                rank: playing[0],
                kicker: kickers[0],
            },
            6 => Flush {
                ranks: playing.try_into().unwrap(),
            },
            5 => Straight { rank: playing[0] },
            4 => ThreeOfAKind {
                rank: playing[0],
                kickers: kickers.try_into().unwrap(),
            },
            3 => TwoPair {
                ranks: playing.try_into().unwrap(),
                kicker: kickers[0],
            },
            2 => OnePair {
                rank: playing[0],
                kickers: kickers.try_into().unwrap(),
            },
            1 => NoPair {
                ranks: playing.try_into().unwrap(),
            },
            _ => unreachable!(),
        }
    }

    // Algorithm inspired by poker-eval EVAL_N (https://github.com/v2k/poker-eval/)
    pub fn of(hand: CardSet) -> Evaluation {
        static POPCNT: Lazy<[u8; 1 << Rank::COUNT]> = Lazy::new(|| {
            let mut x = [0u8; 1 << Rank::COUNT];
            for (i, x) in x.iter_mut().enumerate() {
                *x = i.count_ones() as u8;
            }
            x
        });

        static FIND_STRAIGHT: Lazy<[Option<NonZeroU8>; 1 << Rank::COUNT]> = Lazy::new(|| {
            let find = |mask: u32| {
                for high in (4..Rank::COUNT as u8).rev() {
                    if (mask >> (high - 4)) & 0x1f == 0x1f {
                        return Some(NonZeroU8::new(high).unwrap());
                    }
                }
                if (mask & (1 << (Rank::COUNT - 1)) > 0) && (mask & 0xf == 0xf) {
                    return Some(NonZeroU8::new(3).unwrap());
                }
                None
            };

            let mut x = [None; 1 << Rank::COUNT];
            for (i, x) in x.iter_mut().enumerate() {
                *x = find(i as u32);
            }
            x
        });

        // TOP_MASK_N[N<5][mask] = mask with only the top N+1 bits set
        static TOP_MASK_N: Lazy<[[u32; 1 << Rank::COUNT]; 5]> = Lazy::new(|| {
            let mut x = [[0u32; 1 << Rank::COUNT]; 5];
            for i in 1u32..(1 << Rank::COUNT) {
                x[0][i as usize] = 1 << (31 - i.leading_zeros());
            }
            for j in 1..5 {
                for i in 1..(1 << Rank::COUNT) {
                    x[j][i] = x[j - 1][i] ^ x[0][i ^ x[j - 1][i] as usize];
                }
            }
            x
        });

        let cards = hand.cards() as u32;
        // mask of ranks present by suit
        let by_suit = {
            let mut x = [0u32; Suit::COUNT];
            for (s, x) in x.iter_mut().enumerate() {
                *x = ((hand.mask >> (s * Rank::COUNT)) & ((1 << Rank::COUNT) - 1)) as u32;
            }
            x
        };

        // mask of ranks present in any suit
        let any_suit = by_suit.iter().fold(0, u32::bitor);
        let distint_count = POPCNT[any_suit as usize] as u32;
        let dup_count = cards - distint_count;

        // maximum for flush/straight/both
        let mut max_fs = 0;

        for s in 0..Suit::COUNT {
            // try flush
            if POPCNT[by_suit[s] as usize] >= 5 {
                if let Some(high) = FIND_STRAIGHT[by_suit[s] as usize] {
                    // straight flush - if we are guaranteed the deck is fewer than 10 cards
                    // could return now
                    max_fs = max_fs.max((9 << 28) + (1 << (high.get() + 13)));
                } else {
                    // only flush
                    max_fs = max_fs.max((6 << 28) + (TOP_MASK_N[4][by_suit[s] as usize] << 13));
                }
            }
        }

        // try straight
        if let Some(high) = FIND_STRAIGHT[any_suit as usize] {
            max_fs = max_fs.max((5 << 28) + (1 << (high.get() + 13)));
        }

        let odd_ranks = by_suit.iter().fold(0, u32::bitxor);
        // equivalent to any_suit & !odd_ranks since (!any_suit & odd_ranks) == 0
        let even_ranks = any_suit ^ odd_ranks;
        // all code apart from three_ranks and four_ranks should work correctly independently of the
        // number of suits
        let three_ranks = ((by_suit[0] & by_suit[1]) | (by_suit[2] & by_suit[3]))
            & ((by_suit[0] & by_suit[2]) | (by_suit[1] & by_suit[3]));
        let four_ranks = by_suit.iter().fold((1 << Rank::COUNT) - 1, u32::bitand);

        // maximum for multi-card combo
        let max_combo = match dup_count {
            0 => {
                // high card
                (1 << 28) + (TOP_MASK_N[4][any_suit as usize] << 13)
            }

            1 => {
                // one pair
                let top = even_ranks;
                (2 << 28) + (top << 13) + (TOP_MASK_N[2][(any_suit ^ top) as usize])
            }

            2 => {
                if even_ranks == 0 {
                    // three of a kind
                    (4 << 28)
                        + (three_ranks << 13)
                        + (TOP_MASK_N[1][(any_suit ^ three_ranks) as usize])
                } else {
                    // two pair
                    let pairs = even_ranks;
                    (3 << 28) + (pairs << 13) + (TOP_MASK_N[0][(any_suit ^ pairs) as usize])
                }
            }

            _ => {
                if four_ranks > 0 {
                    // four of a kind
                    // if number of cards is limited to 7, only one quad is possible and finding the
                    // top one would be unnecessary
                    let top = TOP_MASK_N[0][four_ranks as usize];
                    (8 << 28) + (top << 13) + (TOP_MASK_N[0][(any_suit ^ top) as usize])
                } else if three_ranks > 0 {
                    // full house
                    let top = TOP_MASK_N[0][three_ranks as usize];
                    // kicker must be at least pair but might also be another triplet
                    let kicker = TOP_MASK_N[0][((three_ranks | even_ranks) ^ top) as usize];
                    (7 << 28) + (top << 13) + (kicker)
                } else {
                    // two pair (out of three or more)
                    let pairs = TOP_MASK_N[1][even_ranks as usize];
                    (3 << 28) + (pairs << 13) + (TOP_MASK_N[0][(any_suit ^ pairs) as usize])
                }
            }
        };

        Evaluation {
            val: u32::max(max_fs, max_combo),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, EnumDiscriminants)]
pub enum EvaluationDecoded {
    NoPair { ranks: [Rank; 5] },
    OnePair { rank: Rank, kickers: [Rank; 3] },
    TwoPair { ranks: [Rank; 2], kicker: Rank },
    ThreeOfAKind { rank: Rank, kickers: [Rank; 2] },
    Straight { rank: Rank },
    Flush { ranks: [Rank; 5] },
    FullHouse { rank: Rank, kicker: Rank },
    FourOfAKind { rank: Rank, kicker: Rank },
    StraightFlush { rank: Rank },
}

impl Display for EvaluationDecoded {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use EvaluationDecoded::*;
        match self {
            NoPair { ranks } => write!(
                f,
                "high card {} with {},{},{},{} kickers",
                ranks[0],
                ranks[1].abbr(),
                ranks[2].abbr(),
                ranks[3].abbr(),
                ranks[4].abbr()
            ),
            OnePair { rank, kickers } => write!(
                f,
                "pair of {}s with {},{},{} kickers",
                rank,
                kickers[0].abbr(),
                kickers[1].abbr(),
                kickers[2].abbr()
            ),
            TwoPair { ranks, kicker } => write!(
                f,
                "pairs of {}s and {}s with {} kicker",
                ranks[0],
                ranks[1],
                kicker.abbr()
            ),
            ThreeOfAKind { rank, kickers } => write!(
                f,
                "three {}s with {},{} kickers",
                rank,
                kickers[0].abbr(),
                kickers[1].abbr()
            ),
            Straight { rank } => write!(f, "{} high straight", rank),
            Flush { ranks } => write!(
                f,
                "flush of {},{},{},{},{}",
                ranks[0].abbr(),
                ranks[1].abbr(),
                ranks[2].abbr(),
                ranks[3].abbr(),
                ranks[4].abbr()
            ),
            FullHouse { rank, kicker } => write!(f, "{}s full of {}s", rank, kicker),
            FourOfAKind { rank, kicker } => write!(f, "four {}s with {} kicker", rank, kicker),
            StraightFlush { rank } => write!(f, "{} high straight flush", rank),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Hand {
    Pair(Rank),
    Suited([Rank; 2]),
    Offsuit([Rank; 2]),
}

impl Hand {
    pub fn new(first: Card, second: Card) -> Hand {
        let suited = first.suit() == second.suit();
        let (high, low) = (
            first.rank().max(second.rank()),
            first.rank().min(second.rank()),
        );
        if high == low {
            Hand::Pair(high)
        } else if suited {
            Hand::Suited([high, low])
        } else {
            Hand::Offsuit([high, low])
        }
    }

    pub fn from_grid_pos(x: u8, y: u8) -> Option<Hand> {
        if x < 13 && y < 13 {
            let (xr, yr) = (
                Rank::try_from(12 - x).unwrap(),
                Rank::try_from(12 - y).unwrap(),
            );
            Some(match x.cmp(&y) {
                Ordering::Equal => Hand::Pair(xr),
                Ordering::Less => Hand::Offsuit([xr, yr]),
                Ordering::Greater => Hand::Suited([yr, xr]),
            })
        } else {
            None
        }
    }

    pub fn abbr(&self) -> impl Display {
        struct Tmp(Hand);
        impl Display for Tmp {
            fn fmt(&self, f: &mut Formatter) -> fmt::Result {
                match self.0 {
                    Hand::Pair(r) => write!(f, "{0}{0}", r.abbr()),
                    Hand::Suited([h, l]) => write!(f, "{}{}s", h.abbr(), l.abbr()),
                    Hand::Offsuit([h, l]) => write!(f, "{}{}o", h.abbr(), l.abbr()),
                }
            }
        }
        Tmp(*self)
    }

    pub fn combos(&self) -> usize {
        match self {
            Hand::Pair(_) => 6,
            Hand::Suited(_) => 4,
            Hand::Offsuit(_) => 12,
        }
    }

    pub fn iter() -> impl Iterator<Item = Hand> {
        (0u8..169).map(|i| Hand::from_grid_pos(i / 13, i % 13).unwrap())
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct HandMap<T>(HashMap<Hand, T>);

impl<T> HandMap<T> {
    pub fn new() -> Self
    where
        T: Default,
    {
        let mut map = HashMap::new();
        for h in Hand::iter() {
            map.insert(h, T::default());
        }
        HandMap(map)
    }

    pub fn get(&self, hand: &Hand) -> &T {
        self.0.get(hand).unwrap()
    }
    pub fn get_mut(&mut self, hand: &Hand) -> &mut T {
        self.0.get_mut(hand).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::{EvaluationDecoded::*, Rank::*, *};

    fn cs(repr: &str) -> CardSet {
        let mut set = CardSet::empty();
        let mut r = None;
        for tmp in repr.chars() {
            match r.take() {
                None => r = Some(tmp),
                Some(r) => {
                    set += Card::new(
                        Rank::from_abbr(r).expect("invalid rank"),
                        Suit::from_abbr(tmp).expect("invalid suit"),
                    )
                }
            }
        }
        set
    }

    #[test]
    fn minimal() {
        assert_eq!(
            Evaluation::of(cs("Kh8d7d5hTs")).decode(),
            NoPair {
                ranks: [King, Ten, Eight, Seven, Five]
            }
        );
        assert_eq!(
            Evaluation::of(cs("AsAdKsQh7d")).decode(),
            OnePair {
                rank: Ace,
                kickers: [King, Queen, Seven]
            }
        );
        assert_eq!(
            Evaluation::of(cs("2d2sQdQh5d")).decode(),
            TwoPair {
                ranks: [Queen, Two],
                kicker: Five
            }
        );
        assert_eq!(
            Evaluation::of(cs("QdQh3d3h2s")).decode(),
            TwoPair {
                ranks: [Queen, Three],
                kicker: Two
            }
        );
        assert_eq!(
            Evaluation::of(cs("Td2hTsThQdKh")).decode(),
            ThreeOfAKind {
                rank: Ten,
                kickers: [King, Queen]
            }
        );
        assert_eq!(
            Evaluation::of(cs("3h4s2d6h5cAc")).decode(),
            Straight { rank: Six },
        );
        assert_eq!(
            Evaluation::of(cs("3h4s2d5cAc")).decode(),
            Straight { rank: Five },
        );
        assert_eq!(
            Evaluation::of(cs("3h4h7hQh2hQsQc")).decode(),
            Flush {
                ranks: [Queen, Seven, Four, Three, Two]
            }
        );
        assert_eq!(
            Evaluation::of(cs("QsQdQhKsKdKc")).decode(),
            FullHouse {
                rank: King,
                kicker: Queen
            },
        );
        assert_eq!(
            Evaluation::of(cs("KsKdQsKcQdQh")).decode(),
            FullHouse {
                rank: King,
                kicker: Queen
            },
        );
        assert_eq!(
            Evaluation::of(cs("KsQsKcQdQh")).decode(),
            FullHouse {
                rank: Queen,
                kicker: King
            },
        );
        assert_eq!(
            Evaluation::of(cs("4s4d4c8h4hAs")).decode(),
            FourOfAKind {
                rank: Four,
                kicker: Ace
            }
        );
        assert_eq!(
            Evaluation::of(cs("AhQhJhThTdTsKh")).decode(),
            StraightFlush { rank: Ace },
        );
        // both straight and flush but not straight flush
        assert_eq!(
            Evaluation::of(cs("QsKs8s6sTsJh9h")).decode(),
            Flush {
                ranks: [King, Queen, Ten, Eight, Six]
            },
        );
        // pick top two out of three pairs
        assert_eq!(
            Evaluation::of(cs("Qs6h6s8dQd8sAh")).decode(),
            TwoPair {
                ranks: [Queen, Eight],
                kicker: Ace
            }
        );
        // more than 5 cards of the same suit
        assert_eq!(
            Evaluation::of(cs("KhQhTh8h5h3h6d")).decode(),
            Flush {
                ranks: [King, Queen, Ten, Eight, Five]
            }
        );
    }

    #[test]
    fn random() {
        assert_eq!(
            Evaluation::of(cs("Jd7h4h4dJh8d7d")).decode(),
            TwoPair {
                ranks: [Jack, Seven],
                kicker: Eight
            }
        );
        assert_eq!(
            Evaluation::of(cs("7h8h8sKd6cAs4c")).decode(),
            OnePair {
                rank: Eight,
                kickers: [Ace, King, Seven]
            }
        );
        assert_eq!(
            Evaluation::of(cs("TdTsAdQh6s4s5s")).decode(),
            OnePair {
                rank: Ten,
                kickers: [Ace, Queen, Six]
            }
        );
        assert_eq!(
            Evaluation::of(cs("8d9d5sQh2d2h2s")).decode(),
            ThreeOfAKind {
                rank: Two,
                kickers: [Queen, Nine]
            }
        );
        assert_eq!(
            Evaluation::of(cs("Jd7d9s3c2d3d7c")).decode(),
            TwoPair {
                ranks: [Seven, Three],
                kicker: Jack
            }
        );
        assert_eq!(
            Evaluation::of(cs("Qs4dJhQhQd4h3c")).decode(),
            FullHouse {
                rank: Queen,
                kicker: Four
            }
        );
        assert_eq!(
            Evaluation::of(cs("9s6d7c4sKs4dJh")).decode(),
            OnePair {
                rank: Four,
                kickers: [King, Jack, Nine]
            }
        );
        assert_eq!(
            Evaluation::of(cs("Qs7cKcAh2cJcQc")).decode(),
            Flush {
                ranks: [King, Queen, Jack, Seven, Two]
            }
        );
    }
}
